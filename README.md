# Image Insight


## Overview

Pass a picture URL to the Web app and it'll be passed, via a taskk queue to a worker that will uses the Google Vision API to analyzis it.
The result will be passed back to the Web App via Redis.
The number of worker can easily be increased to cope with more load.


## Out of scope
- Error handling
- Validation
- Security, inc. User auth/ID (eg. email)
- User friendly UI

## Stack used

- Google Container Engine & K8S (after 1.5 day lost on Azure!)
- Python / Flask / psq
- Redis
- Tensorflow / Google Vision API
- Existing code
- Zoom, StackOverflow & Google

## Post-morterm

To do!